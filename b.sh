echo "started compiling"
avr-gcc -g -Os -mmcu=atmega2560 -c integrate.c
avr-gcc -g -mmcu=atmega2560 -o integrate.elf integrate.o
avr-objcopy -j .text -j .data -O ihex integrate.elf integrate.hex

sudo avrdude -c avrispmkII -p atmega2560 -U flash:w:integrate.hex
echo "done"

