#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
//#include <unistd.h>

/*******************UART FUNCTIONS AND DEFINITIONS************************************/
#define RX  (1<<4)
#define TX  (1<<3)
#define TE  (1<<5)
#define RE  (1<<7)
/*******************uart0****************/





void uart0_init()
{
	UCSR0B = 0x00;							//disable while setting baud rate

	UBRR0L = 95; 							// for the clock frequency 14745600 and the baud rate 9600, value of UBRR is 95
	UBRR0H = 0x00;

	UCSR0C = (1<<UCSZ01)|(1<<UCSZ00);							//setting 8-bit character and 1 stop bit
	UCSR0B = RX | TX | RE;						//enabling receiver and transmit
}

void uart_tx0(char data)
{
	while(!(UCSR0A & TE));						//waiting to transmit
	UDR0 = data;
}

char uart_rx0()
{
	while(!(UCSR0A & RE));						//waiting to receive
	return UDR0;
}

void uart_tx0_array(const char *array,int arr_length)
{
	int i=0;
	while(i < arr_length)
	{
		uart_tx0(array[i++]);
	}
}
void uart_tx0_string(char *data)
{
	while(*data != '\0')
	{
		uart_tx0(*data);
		data++;
	}
}

/*******************uart1****************/
void uart1_init()
{
	UCSR1B = 0x00;							//disable while setting baud rate

	UBRR1L = 95; 							// for the clock frequency 14745600 and the baud rate 9600, value of UBRR is 95
	UBRR1H = 0x00;

	UCSR1C = (1<<UCSZ11)|(1<<UCSZ10);							//setting 8-bit character and 1 stop bit
	UCSR1B = RX | TX | RE;						//enabling receiver and transmit
}

void uart_tx1(char data)
{
	while(!(UCSR1A & TE));						//waiting to transmit
	UDR1 = data;
}

char uart_rx1()
{
	while(!(UCSR1A & RE));						//waiting to receive
	return UDR1;
}

void uart_tx1_array(const char *array,int arr_length)
{
	int i=0;
	while(i < arr_length)
	{
		uart_tx1(array[i++]);
	}
}
void uart_tx1_string(char *data)
{
	while(*data != '\0')
	{
		uart_tx1(*data);
		data++;
	}
}

/*******************uart2****************/
void uart2_init()
{
	UCSR2B = 0x00;							//disable while setting baud rate

	UBRR2L = 95; 							// for the clock frequency 14745600 and the baud rate 9600, value of UBRR is 95
	UBRR2H = 0x00;

	UCSR2C = (1<<UCSZ21)|(1<<UCSZ20);							//setting 8-bit character and 1 stop bit
	UCSR2B = RX | TX | RE;						//enabling receiver and transmit
						//enabling receiver and transmit
}

void uart_tx2(char data)
{
	while(!(UCSR2A & TE));						//waiting to transmit
	UDR2 = data;
}

char uart_rx2()
{
	while(!(UCSR2A & RE));						//waiting to receive
	return UDR2;
}

void uart_tx2_array(const char *array,int arr_length)
{
	int i=0;
	while(i < arr_length)
	{
		uart_tx2(array[i++]);
	}
}
void uart_tx2_string(char *data)
{
	while(*data != '\0')
	{
		uart_tx2(*data);
		data++;
	}
}
/******______**
void clk(unsigned int t)
{
	unsigned int i;
	for(i=0;i<t;i++)
	{
		PORTA=0b00000001;
		_delay_us(20);
		PORTA=0b00000100;
		_delay_us(20);
		PORTA=0b00000010;
		_delay_us(20);
		PORTA=0b00001000;
		_delay_us(20);
	}
}
void aclk(unsigned int t)
{
	unsigned int i;
	for(i=0;i<t;i++)
	{
		PORTA=0b00001000;
		_delay_us(20);
		PORTA=0b00000010;
		_delay_us(20);
		PORTA=0b00000100;
		_delay_us(20);
		PORTA=0b00000001;
		_delay_us(20);
	}
}

/********Stepper Motor NEds*******/

unsigned int mj=0,pj=0;
ISR(USART1_RX_vect)
{

	if((mj%12)==0)
	uart_tx0('X');
	uart_tx0(UDR1);
	mj++;
}

ISR(USART2_RX_vect)
{

	if((pj%12)==0)
	uart_tx0('Y');
	uart_tx0(UDR2);
	pj++;
}

int main(void)
{
	// DDRA=0xFF;
	// unsigned int t;
	// char temp;
	// t=200;
	// //clk(t);
	// _delay_ms(20);
	// //aclk(t);
	// sei();
	 uart0_init();//connected to PC
	// uart1_init();//connected to GSM
	// uart2_init();

	//uart_tx0('1');


	
	

	uart_tx0_string("AT+CMGF=1\r");
	_delay_ms(1000);
	uart_tx0_string("AT+CMGS=\"+917588505049\"\r");
	_delay_ms(1000);
	uart_tx0_string("hi");
	_delay_ms(1000);
	uart_tx0(26);
	_delay_ms(1000);
//uart_tx1(13);
	
	
	
}


