// #ifndef F_CPU
// #define F_CPU 14745600
// #endif

// #include <avr/io.h>
// #include <util/delay.h>
// #include <avr/interrupt.h>


// unsigned char card[20];
// unsigned int i=0,j=0,k;

// void uart0_init(uint32_t baud)
// {
	
// 	uint16_t ubrr = F_CPU / 16 / baud -1;
	
// 	UBRR0H = (unsigned char)(ubrr>>8);
// 	UBRR0L = (unsigned char)ubrr;
	
// 	UCSR0B = (1<<RXEN0)|(1<<TXEN0)|(1<<RXCIE0);
	
// 	UCSR0C = (3<<UCSZ00);
// }

// void uart0_tx(uint8_t data)
// {
// 	while(!(UCSR0A & (1 << UDRE0)));
// 	UDR0 = data;
// }

// uint8_t uart0_rx()
// {
// 	while(!(UCSR0A & (1<<RXC0)));
// 	return UDR0;
// }

// void uart1_init(uint32_t baud)
// {
	
// 	uint16_t ubrr = F_CPU / 16 / baud -1;
	
// 	UBRR1H = (unsigned char)(ubrr>>8);
// 	UBRR1L = (unsigned char)ubrr;
	
// 	UCSR1B = (1<<RXEN1)|(1<<TXEN1)|(1<<RXCIE1);
	
// 	UCSR1C = (3<<UCSZ10);
// }

// uint8_t uart1_rx()
// {
// 	while(!(UCSR1A & (1<<RXC1)));
// 	return UDR1;
// }

// void uart1_tx(uint8_t data)
// {
// 	while(!(UCSR1A & (1 << UDRE1)));
// 	UDR1 = data;
// }

// void uart2_init(uint32_t baud)
// {
	
// 	uint16_t ubrr = F_CPU / 16 / baud -1;
	
// 	UBRR2H = (unsigned char)(ubrr>>8);
// 	UBRR2L = (unsigned char)ubrr;
	
// 	UCSR2B = (1<<RXEN2)|(1<<TXEN2)|(1<<RXCIE2);
	
// 	UCSR2C = (3<<UCSZ20);
// }

// void uart2_tx(uint8_t data)
// {
// 	while(!(UCSR2A & (1 << UDRE2)));
// 	UDR2 = data;
// }

// uint8_t uart2_rx()
// {
// 	while(!(UCSR2A & (1<<RXC2)));
// 	return UDR2;
// }

// ISR(USART0_RX_vect)
// {
// 		//j=UDR0;
// 	unsigned char abc;
// 	abc=UDR0;
// 	uart2_tx(abc); 
	
// 	//uart2_tx('a');
//  //    if(i<12)
//  //    {
//  //    	uart2_tx(UDR0);
//  //    	//card[i]=UDR0;
//  //    	i++;
//  //    	//PORTA=(0xff)^(PORTA);
//  //    	//_delay_ms(100);
//  //    	//uart2_tx(card[i]);
//  //    }
//  //    else 
//  //    {
//  //    //	uart2_tx('b');
//  //    	// for(k=0;k<12;k++)
//  //    	// {
//  //    	// //	uart2_tx(card[i]);
//  //    	// 	PORTA=card[i];
//  //    	// }
//  //    	// i=0;
//  //    }
// 	// //reti();
// }

// // ISR(USART2_RX_vect)
// // {
// // 	uart2_tx('c');
// //     // if(j<12)
// //     // 	card2[j]=UDR2;
// //     // else 
// //     // {
// //     // 	uart1_tx('y');
// //     // 	for(k=0;k<12;k++)
// //     // 		uart1_tx(card2[j]);
// //     // }
// //     // j++;
// // 	//reti();
// // }

// int main()
// {
// 	// char data;
// 	// uint8_t b[10];
// 	// char card[20],a[]="read fghcard";
// 	// int i=0,j=0;
// 	DDRA=0xFF;
// 	uart0_init(9600);
//  	uart1_init(9600);
//  	uart2_init(9600);
//  	sei();
//  	//  while(1)
//  	//{
//  	 		//uart2_tx('a');
//  	 // 	for(i=0;i<12;i++)
//  	 // 		 	card[i]=uart0_rx();
// 		 // for(i=0;i<12;i++)
//  	 // 		 	uart2_tx(card[i]);
//  	//}
//  	// uart2_tx('1');
//  	while(1);
//  	return 0;
// }

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

/*******************UART FUNCTIONS AND DEFINITIONS************************************/
#define RX  (1<<4)
#define TX  (1<<3)
#define TE  (1<<5)
#define RE  (1<<7)
/*******************uart0****************/


unsigned int i=0;


void uart0_init()
{
	UCSR0B = 0x00;							//disable while setting baud rate

	UBRR0L = 95; 							// for the clock frequency 14745600 and the baud rate 9600, value of UBRR is 95
	UBRR0H = 0x00;

	UCSR0C = (1<<UCSZ01)|(1<<UCSZ00);							//setting 8-bit character and 1 stop bit
	UCSR0B = RX | TX | RE;						//enabling receiver and transmit
}

void uart_tx0(char data)
{
	while(!(UCSR0A & TE));						//waiting to transmit
	UDR0 = data;
}

char uart_rx0()
{
	while(!(UCSR0A & RE));						//waiting to receive
	return UDR0;
}

void uart_tx0_array(const char *array,int arr_length)
{
	int i=0;
	while(i < arr_length)
	{
		uart_tx0(array[i++]);
	}
}
void uart_tx0_string(char *data)
{
	while(*data != '\0')
	{
		uart_tx0(*data);
		data++;
	}
}

/*******************uart1****************/
void uart1_init()
{
	UCSR1B = 0x00;							//disable while setting baud rate

	UBRR1L = 95; 							// for the clock frequency 14745600 and the baud rate 9600, value of UBRR is 95
	UBRR1H = 0x00;

	UCSR1C = (1<<UCSZ11)|(1<<UCSZ10);							//setting 8-bit character and 1 stop bit
	UCSR1B = RX | TX | RE;						//enabling receiver and transmit
}

void uart_tx1(char data)
{
	while(!(UCSR1A & TE));						//waiting to transmit
	UDR1 = data;
}

char uart_rx1()
{
	while(!(UCSR1A & RE));						//waiting to receive
	return UDR1;
}

void uart_tx1_array(const char *array,int arr_length)
{
	int i=0;
	while(i < arr_length)
	{
		uart_tx1(array[i++]);
	}
}
void uart_tx1_string(char *data)
{
	while(*data != '\0')
	{
		uart_tx1(*data);
		data++;
	}
}

/*******************uart2****************/
void uart2_init()
{
	UCSR2B = 0x00;							//disable while setting baud rate

	UBRR2L = 95; 							// for the clock frequency 14745600 and the baud rate 9600, value of UBRR is 95
	UBRR2H = 0x00;

	UCSR2C = (1<<UCSZ21)|(1<<UCSZ20);							//setting 8-bit character and 1 stop bit
	UCSR2B = RX | TX | RE;						//enabling receiver and transmit
}

void uart_tx2(char data)
{
	while(!(UCSR2A & TE));						//waiting to transmit
	UDR2 = data;
}

char uart_rx2()
{
	while(!(UCSR2A & RE));						//waiting to receive
	return UDR2;
}

void uart_tx2_array(const char *array,int arr_length)
{
	int i=0;
	while(i < arr_length)
	{
		uart_tx2(array[i++]);
	}
}
void uart_tx2_string(char *data)
{
	while(*data != '\0')
	{
		uart_tx2(*data);
		data++;
	}
}
/******______**
void clk(unsigned int t)
{
	unsigned int i;
	for(i=0;i<t;i++)
	{
		PORTA=0b00000001;
		_delay_us(20);
		PORTA=0b00000100;
		_delay_us(20);
		PORTA=0b00000010;
		_delay_us(20);
		PORTA=0b00001000;
		_delay_us(20);
	}
}
void aclk(unsigned int t)
{
	unsigned int i;
	for(i=0;i<t;i++)
	{
		PORTA=0b00001000;
		_delay_us(20);
		PORTA=0b00000010;
		_delay_us(20);
		PORTA=0b00000100;
		_delay_us(20);
		PORTA=0b00000001;
		_delay_us(20);
	}
}

/********Stepper Motor NEds*******/


ISR(USART1_RX_vect)
{
	uart_tx0(UDR1);
}

ISR(USART2_RX_vect)
{
    	uart_tx0(UDR2);
}

int main(void)
{
	DDRA=0xFF;
	unsigned int t;
	t=200;
	//clk(t);
	_delay_ms(20);
	//aclk(t);
	sei();
	uart0_init();//connected to PC
	uart1_init();//connected to RFID1
	uart2_init();//connect to RFID2
	//while(1);
	uart_tx0('M');
/*
	

	//while(1);

	

	uart_tx1_string("AT+CMGF=1\r");
	_delay_ms(1000);
	uart_tx1_string("AT+CMGS=\"+919822236188\"\r");
	_delay_ms(1000);
	uart_tx1_string("hi");
	_delay_ms(1000);
	uart_tx1(26);
	_delay_ms(1000);

	//uart_tx1(13);
*/	
	
	
}


